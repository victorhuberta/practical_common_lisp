;;;; Chapter 7 - Macros: Standard Control Constructs

;;; WHEN and UNLESS
;; PROGN is used for executing a list of forms in sequence.
(progn (format t "hello, world!") (+ 1 1))
;; WHEN = IF x PROGN
(when (> 3 2)
  (format t "Executing...")
  (- 1 2))
;; and it's defined like this:
(defmacro when-1 (cond &rest body)
  `(if ,cond (progn ,@body)))
;; UNLESS = IF-NOT x PROGN
(unless (< 3 2)
  (format t "Executing...")
  (- 1 2))
;; and it's defined like this:
(defmacro unless-1 (cond &rest body)
  `(if (not ,cond) (progn ,@body)))


;;; COND for multibranch conditionals.
(let ((x 5))
  (cond
    ((> x 10) (format t "Bigger than ten."))
    ((< x 4) (format t "Smaller than four."))
    (t (format t "~d can't be categorized." 5))))


;;; AND, OR, NOT
(not (= 1 1)) ; NOT is not (ha!) a macro
(and (= 1 2) (= 3 3))
(or (= 1 2) (= 3 3))


;;; Looping constructs are macros built on top of two goto special operators.
;; DOLIST and DOTIMES
(dolist (x '(1 2 3))
  (if (evenp x) (return) (print x)))
(dotimes (x 10)
  (print x))
;; Nested DOTIMES loops
(dotimes (a 20)
  (dotimes (b 20)
    (format t "~3d " (* (1+ a) (1+ b))))
  (format t "~%"))
;; DO construct:
;; (do (variable-definition*)
;;     (end-test-form result-form*)
;;   statement*)
;;
;; variable-definition has the form of:
;; (var init-form step-form)
;;
;; DO evaluates all step forms before assigning each variable its value.
;; DO* assigns each variable its value before evaluating the step form for
;; subsequent variables.
;;
;; Examples:
;; Return the 11th Fibonacci number.
(do ((n 0 (1+ n))
     (cur 0 next) ; the referred next is the old one
     (next 1 (+ cur next))) ; the referred cur is the old one
    ((= 10 n) cur)) ; statement is optional
;; Print 0 to 3.
(do ((i 0 (1+ i)))
    ((>= i 4)) ; result-form is optional
  (print i))
;; Wait for some future date.
(do () ; variable-definition is optional
    ((> (get-universal-time) *some-future-date*))
  (format t "Waiting~%")
  (sleep 60))
;;
;; LOOP enables looping for many data structures, e.g., lists, vectors,
;; hash tables, and packages.
;;
;; It also enables accumulating values, e.g., collecting, counting, summing,
;; minimizing, maximizing.
;;
;; Simple LOOPs:
(loop
  (when (> (get-universal-time) *some-future-date*)
    (return))
  (format t "Waiting ...~%")
  (sleep 1))
;; Extended LOOPs have keywords that are parsed according to loop’s grammar.
;; Examples:
;; Collect a list of numbers from 1 to 10.
(loop for i from 1 to 10 collecting i)
;; Sum all x^2 where x is from 1 to 10.
(loop for x from 1 to 10 summing (expt x 2))
;; Count the number of vowels in a sentence.
(loop for x across "the quick brown fox jumps over the lazy dog"
  counting (find x "aeiou"))
;; Return the 11th Fibonacci number.
(loop for i below 10
  and a = 0 then b
  and b = 1 then (+ b a)
  finally (return a))
