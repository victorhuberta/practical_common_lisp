;;;; Chapter 5 - Functions

;;; Example of a function with documentation string
(defun verbose-sum (x y)
  "Sum any two numbers after printing a message."
  (format t "Summing ~d and ~d.~%" x y)
  (+ x y))


;;; Conversion function naming convention
(defun num->string (n)
  (format nil "~d" n))


;;; Optional parameters
(defun foo (a b &optional c d)
  (list a b c d))
;; Default arguments
(defun legal-girl (name &optional (age 18))
  (format t "~a is~alegal." name (if (< age 18) " NOT " " ")))
;; Default-value expressions can refer to earlier parameters.
(defun make-rect (width &optional (height width))
  (if (= width height)
    (format t "It's a square of width ~d." width)
    (format t "It's a rectangle of width ~d and height ~d." width height)))
;; Supplied-p parameters make it clear whether an argument was passed in.
(defun bar (a b &optional (c nil c-supplied-p))
  (list a b c c-supplied-p))


;;; Rest parameters
(defun pass-args (&rest numbers)
  (format t "You passed ~d arguments." (length numbers)))


;;; Keyword parameters
(defun kwargs (a &key b c d)
  (list a b c d))
;; If for some reason the keywords are different from the parameter names...
(defun public-api (&key ((:god g) t) ((:doesnt d) t) ((:exist e)))
  (list g d e))


;;; Avoid combining these types of parameters.
;; &optional + &key
(defun this-is-bad (a &optional b &key c) ; try it at your own risk
  (list a b c))
;; &rest + &key
(defun this-is-surprising (&rest r &key a b c) ; both parameters are filled
  (list r a b c))
;; all of them
(defun oh-no (a &optional b &rest c &key d) ; but this is the right sequence
  (list a b c d))


;;; RETURN-FROM macro breaks out of a block with the given name.
(defun pointless (n)
  (dotimes (i 10)
    (dotimes (j 10)
      (when (> (* i j) n)
        (return-from pointless (list i j))))))


;;; Treating functions as data.
;; function/(#') gets hold of a function object by name.
(defun pound-prime () t)
(equal (function pound-prime) #'pound-prime)
;; funcall is for invoking a function with known number of arguments.
(defun plot (fn min max step)
  (loop for i from min to max by step do
    (loop repeat (funcall fn i) do
      (format t "*"))
    (format t "~%")))
;; apply is for invoking a function with a list of arguments.
(defvar plot-data (list #'exp 1 5 1/2))
(apply #'plot plot-data)
(apply #'plot #'exp (list 1 5 2)) ; you can separate some args from the list
;; LAMBDA expression can be seen as a function name that describes its own body.
(plot #'(lambda (x) (* 2 x)) 1 5 1)
