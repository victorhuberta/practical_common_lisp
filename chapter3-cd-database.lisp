;;;; Chapter 3 - Practical: A Simple Database

(defvar *db* nil)

(defun make-cd (title artist rating ripped)
  (list :title title :artist artist :rating rating :ripped ripped))

(defun add-record (cd)
  (push cd *db*))

(defun dump-db ()
  (dolist (cd *db*)
    (format t "~{~a:~10t~a~%~}~%" cd)))

(defun prompt-read (prompt)
  (format *query-io* "~a: " prompt)
  (force-output *query-io*)
  (read-line *query-io*))

(defun prompt-for-cd ()
  (format t "~%")
  (make-cd (prompt-read "Title")
           (prompt-read "Artist")
           (or (parse-integer (prompt-read "Rating") :junk-allowed t) 0)
           (y-or-n-p "Ripped")))

(defun add-cds ()
  (loop (add-record (prompt-for-cd))
    (if (not (y-or-n-p "Another?")) (return))))

(defun save-db (filename)
  (with-open-file (out filename
                   :direction :output
                   :if-exists :supersede)
    (with-standard-io-syntax
      (print *db* out))))

(defun load-db (filename)
  (with-open-file (in filename)
    (with-standard-io-syntax
      (setf *db* (read in)))))

(defun select-one (prop val)
  (dolist (rec *db*)
    (if (string-equal (format nil "~a" (getf rec prop))
                      (format nil "~a" val))
      (return rec))))

(defun select (selector-fn)
  (remove-if-not selector-fn *db*))

;; (#') means the following expression is a function (and not a variable).
(defun artist-selector (artist)
  #'(lambda (r) (equal (getf r :artist) artist)))

;;; &key means keyword arguments, and
;;; ripped-p is called the supplied-p parameter.
(defun where (&key title artist rating (ripped nil ripped-p))
  #'(lambda (r)
      (and (if title (equal (getf r :title) title) t)
           (if artist (equal (getf r :artist) artist) t)
           (if rating (equal (getf r :rating) rating) t)
           (if ripped-p (equal (getf r :ripped) ripped) t))))

(defun update (selector-fn &key title artist rating (ripped nil ripped-p))
  (setf *db*
    (mapcar
      #'(lambda (r)
          (when (funcall selector-fn r)
            (if title (setf (getf r :title) title))
            (if artist (setf (getf r :artist) artist))
            (if rating (setf (getf r :rating) rating))
            (if ripped-p (setf (getf r :ripped) ripped)))
          r)
      *db*)))

(defun delete-rows (selector-fn)
  (setf *db* (remove-if selector-fn *db*)))

(defmacro backwards (expr) (reverse expr))

;; a backquote (`) defers evaluation of the expression.
;; while a comma (,) evaluates a part of the expression.
(defun make-clause (field value)
  `(equal (getf r ,field) ,value))

(defun collect-clauses (fields)
  (loop while fields
    collecting (make-clause (pop fields) (pop fields))))

;; (,@) splices a list to its enclosing list.
(defmacro where-1 (&rest fields)
  `#'(lambda (r) (and ,@(collect-clauses fields))))

(macroexpand-1 '(where-1 :title "Fly" :ripped t)) ; see the macro expansion

;; a single quote (') defers evaluation of the expression.
(equal '(1 2 3) (list 1 2 3))

(add-record (make-cd "Roses" "Kathy Mattea" 7 t))
(add-record (make-cd "Fly" "Dixie Chicks" 8 t))
(add-record (make-cd "Home" "Dixie Chicks" 9 t))
