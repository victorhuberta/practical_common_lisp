;;;; Chapter 9 - Practical: Building a Unit Test Framework

(defvar *test-name* nil) ; to be bound by test functions

(defmacro combine-results (&rest results)
  "Combine multiple number of test results under a test name."
  (let ((combined (gensym)))
    `(let ((,combined t))
       (if *test-name* (format t "~a:~%" *test-name*))
       ,@(loop for r in results collect `(unless ,r (setf ,combined nil)))
       ,combined)))

(defun report-result (result form)
  "Report the result of a test form."
  (format t "  ~:[FAIL~;pass~] ... ~a~%" result form)
  result)

(defmacro check (&body forms)
  "Run all tests and report each result as well as their combined results."
  `(combine-results
     ,@(loop for f in forms collect `(report-result ,f ',f))))

(defmacro deftest (name params &body body)
  "Define a test function."
  `(defun ,name ,params
     (let ((*test-name* (append *test-name* (list ',name))))
       ,@body)))

(deftest test-+ ()
  "Test for (-) and (+)."
  (check
    (= (1+ 3) 4)
    (= (- 9 3) 6)
    (= (+ 10 99) 109)))

(deftest test/* ()
  "Test for (/) and (*)."
  (check
    (= (* 4 6) 24)
    (= (/ 9 3) 3)
    (= (* 10 99) 990)))

(deftest test-arithmetic ()
  "Test for several arithmetic functions."
  (combine-results
    (test-+)
    (test/*)))
