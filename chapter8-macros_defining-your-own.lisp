;;;; Chapter 8 - Macros: Defining Your Own

;;; Steps to writing a macro are:
;;; 1. Write a sample call to the macro and the code it should expand into,
;;;    or vice versa.
;;; 2. Write code that generates the handwritten expansion from the arguments
;;;    in the sample call.
;;; 3. Make sure the macro abstraction doesn’t “leak.”

(defun primep (x)
  (when (> x 1)
    (loop for f from 2 to (isqrt x) never (zerop (mod x f)))))

(defun next-prime (n)
  (loop for x from n when (primep x) return x))

;; Sample call:
;; (do-primes (p 0 19)
;;   (format t "~d~%" p))
;;
;; What it would expand to:
;; (do ((p (next-prime 0) (next-prime (1+ p))))
;;     ((> p 19))
;;   (format t "~d~%" p))
(defmacro do-primes (range &rest body)
  (let ((var (first range))
        (start (second range))
        (end (third range)))
    `(do ((,var (next-prime ,start) (next-prime (1+ ,var))))
         ((> ,var ,end))
       ,@body)))
;; Even better, do it using destructuring parameter list.
(defmacro do-primes ((var start end) &body body)
  `(do ((,var (next-prime ,start) (next-prime (1+ ,var))))
       ((> ,var ,end))
     ,@body))
;; However, there are abstraction leaks. Do remember these rules of thumb:
;; 1. Thrive to include any subforms in the expansion in positions that will be
;;    evaluated in the same order as the subforms appear in the macro call.
;; 2. Thrive to make sure subforms are evaluated only once by creating a
;;    variable in the expansion to hold the value of evaluating the argument
;;    form.
;; 3. Use GENSYM at macro expansion time to create unique variable names.
(defmacro do-primes ((var start end) &body body)
  (let ((end-val-name (gensym))) ; Rule 3
    `(do ((,var (next-prime ,start) (next-prime (1+ ,var))) ; Rule 1
          (,end-val-name ,end)) ; Rule 2
         ((> ,var ,end-val-name))
       ,@body)))
;; Now, how about a macro-writing macro?
(defmacro with-gensyms ((&rest names) &body body)
  `(let ,(loop for n in names collect `(,n (gensym)))
     ,@body))
;; ONCE-ONLY is a macro-writing macro that evaluates certain macro arguments
;; once only and in a particular order.
(defmacro do-primes ((var start end) &body body)
  (once-only (start end)
    `(do ((,var (next-prime ,start) (next-prime (1+ ,var))))
         ((> ,var ,end))
       ,@body)))
