;;;; Chapter 6 - Variables

;;; Binding forms (e.g., FUNCTION, LET, LET*, DOTIMES)
;; Scoping demonstrated
(defun binding (x)
  (format t "Parameter: ~a~%" x)
  (let ((x (+ x 1)))
    (format t "Outer LET: ~a~%" x)
    (let ((x (+ x 1)))
      (format t "Inner LET: ~a~%" x))
    (format t "Outer LET: ~a~%" x))
  (format t "Parameter: ~a~%" x))
;; LET* let (get it?) you refer to earlier variables.
(defun binding-1 ()
  (let* ((x 10)
         (y (* x 2)))
    (format t "~a and ~a" x y)))
;; DOTIMES holds a counter that is incremented every loop.
(defun repeat (n)
  (dotimes (n n) (format t "~d " n))) ; n is shadowed


;;; Lexical variables can only be referred to by code within the binding form.
;; An anonymous function "closes over" the variable binding (not the value).
(defparameter *closure* (let ((count 0)) #'(lambda () (setf count (1+ count)))))
(funcall *closure*) ; count = 1
(funcall *closure*) ; count = 2
(funcall *closure*) ; count = 3
;; Many closures can refer to the same binding.
(let ((count 0))
  (list
    #'(lambda () (incf count))
    #'(lambda () (decf count))
    #'(lambda () count)))


;;; Dynamic, a.k.a. Special, variables can be referred to from anywhere.
;; DEFVAR only assigns the initial value to the variable if it is undefined.
(defvar *foo* 0
  "It doesn't mean anything.")
;; DEFPARAMETER always assigns the initial value to the variable.
(defparameter *bar* 0.00001
  "It means something.")
;; Dynamic binding: When we bind a dynamic variable, the binding that's created
;; on entry to the binding form replaces the global binding temporarily.
(defparameter *a* 10)

(defun foo ()
  (format t "~d~%" *a*))

(defun dynbind ()
  (foo)
  (let ((*a* 20)) (foo))
  (foo))


;;; Use DEFCONSTANT to define constants.
(defconstant +foobar+ -1 "You can redefine constants; what is the point?")
;; Some key constants are T, NIL, and PI.
(format t "~a~%~a~%~a~%" t nil pi) ; why no follow naming conventions!?


;;; SETF is a general-purpose assignment operator.
;; It replaces the value of a variable binding without affecting the others.
(defun foo (x)
  (setf x 10)
  (format t "~a~%" x))

(defun assign (&optional (x 20) y)
  (foo x) ; doesn't affect the binding of its enclosing scope
  (format t "~a,~a~%" x y)
  (setf x 1 y 2) ; assign to two variables
  (format t "~a,~a~%" x y)
  (setf x (setf y (random 10))) ; SETF returns the newly assigned value
  (format t "~a,~a~%" x y))


;;; Modify macros: Macros built on top of SETF that modify places by assigning
;;; a new value based on the current value of the place (e.g., INCF, PUSH, etc).
;;; Also, each argument to a modify macro is evaluated exactly once; from left
;;; to right.
(let ((x 1) (y 5))
  (incf x) ; = (setf x (+ x 1))
  (format t "~a,~a~%" x y)
  (incf x 10) ; = (setf x (+ x 10))
  (format t "~a,~a~%" x y)
  (decf x) ; = (setf x (- x 1))
  (format t "~a,~a~%" x y)
  (rotatef x y) ; = (let ((tmp x)) (setf x y y tmp) nil)
  (format t "~a,~a~%" x y)
  (shiftf x y 100) ; = (let ((tmp x)) (setf x y y 100) tmp)
  (format t "~a,~a~%" x y))
